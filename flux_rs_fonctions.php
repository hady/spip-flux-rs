<?php
/**
 * Fonctions utiles au plugin Flux des principaux réseaux sociaux
 *
 * @plugin     Flux des principaux réseaux sociaux
 * @copyright  2017
 * @author     Hadrien
 * @licence    GNU/GPL
 * @package    SPIP\Flux_rs\Fonctions
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Récupère le flux facebook d'une page
 *
 * @param int $limit nombre de posts à afficher
 * @return array tableau du flux
**/
function facebook_feed($limit="2"){
	include_spip('lib/facebook-php-sdk-master/src/facebook');
	include_spip('inc/config');

	// connect to app
	$config = array();
	$config['appId'] = lire_config('flux_rs/fb_app_id');
	$config['secret'] = lire_config('flux_rs/fb_secret');
	$config['fileUpload'] = true; // optional

	// instantiate
	$facebook = new Facebook($config);

	// set page id
	$pageid = lire_config('flux_rs/fb_page_id');

	$pagefeed = $facebook->api("/" . $pageid . "/feed?fields=full_picture,message,created_time&limit=" . $limit);

	return $pagefeed['data'];
}

/**
 * Récupère le flux twitter d'une page
 *
 * @param int $count nombre de posts à afficher
 * @return array tableau du flux
**/
function tweeter_feed($count='2'){
	include_spip('inc/config');

	//1 - Settings (please update to math your own)
	$consumer_key=lire_config('flux_rs/tw_key'); //Provide your application consumer key
	$consumer_secret=lire_config('flux_rs/tw_secret'); //Provide your application consumer secret
	$oauth_token = lire_config('flux_rs/tw_token'); //Provide your oAuth Token
	$oauth_token_secret = lire_config('flux_rs/tw_token_secret'); //Provide your oAuth Token Secret

	$screen_name = lire_config('flux_rs/tw_screen_name');

	//2 - Include @abraham's PHP twitteroauth Library
	include_spip('lib/twitteroauth/twitteroauth');
	//3 - Authentication
	/* Create a TwitterOauth object with consumer/user tokens. */
	$connection = new TwitterOAuth($consumer_key, $consumer_secret, $oauth_token, $oauth_token_secret);

	//4 - Start Querying
	// $query = 'https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name='.$screen_name.'&count='.$count.''; //Your Twitter API query
	$query = 'https://api.twitter.com/1.1/statuses/user_timeline.json?count='.$count.'&user_id='.$screen_name.'&screen_name='.$screen_name.'&include_rts=true';
	// &exclude_replies=true
	$content = $connection->get($query);

	$content = object_to_array($content);

	foreach ($content as $key => $value) {
		$content[$key]['created_at'] = date('Y-m-d H:i', strtotime($value['created_at']));
		$content[$key]['text'] = parse($value['text']);
	}

	return $content;
}

/**
 * Parse un post twitter pour ajouter des liens sur des hashtags et des personnes
 *
 * @param string le post twitter
 * @return string le texte parsé
**/
function parse($text) {
	$text = preg_replace('#http://[a-z0-9._/-]+#i', '<a href="$0" class="hover-a c-pu-c" title="En savoir plus">$0</a>', $text);
	$text = preg_replace('#https://[a-z0-9._/-]+#i', '<a href="$0" class="hover-a c-pu-c" title="En savoir plus">$0</a>', $text);
	$text = preg_replace('#@([a-z0-9_]+)#i', '@<a href="http://twitter.com/$1" class="hover-a c-pu-c" title="Visiter la page de cet utilisateur twitter">$1</a>', $text);
	$text = preg_replace('# \#([a-z0-9_-]+)#i', ' #<a href="http://search.twitter.com/search?q=%23$1" class="hover-a c-pu-c" title="Rechercher le hashtag sur twitter">$1</a>', $text);
	return $text;
}

/**
 * Récupère le flux instagram
 *
 * @param int $limit nombre de posts à afficher
 * @return array tableau du flux
**/
function instagram_feed($limit='2'){
	include_spip('lib/Instagram-PHP-API-master/src/instagram');
	include_spip('inc/config');

	$access_token=lire_config('flux_rs/insta_access_token'); //Provide your instagram access token

	$instagram = new Instagram(array(
		'apiKey'      => lire_config('flux_rs/insta_api_key'),
		'apiSecret'   => lire_config('flux_rs/insta_api_secret'),
		'apiCallback' => lire_config('flux_rs/insta_api_callback')
	));

	$instagram->setAccessToken($access_token);
	$media = $instagram->getUserMedia('self',$limit);

	return $media->data;
}

/**
 * Récupère un objet et le converti en array
 *
 * @param objet $obj l'objet
 * @return array l'array
**/
function object_to_array($obj) {
	$_arr = is_object($obj) ? get_object_vars($obj) : $obj;
	foreach ($_arr as $key => $val) {
			$val = (is_array($val) || is_object($val)) ? object_to_array($val) : $val;
			$arr[$key] = $val;
	}
	return $arr;
}
