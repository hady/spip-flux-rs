### Flux des principaux réseaux sociaux


#### Facebook

1. Créer une nouvelle application avec un compte developpeur ici https://developers.facebook.com/apps/
2. Récupérer l'appId ainsi que le secret de cette application
3. Renseigner ces infos dans /ecrire/?exec=configurer_flux_rs
4. Récupérer la pageId de la page à récupérer
5. Le renseigner dans /ecrire/?exec=configurer_flux_rs
6. Le flux devrait remonter

=> L'API Graph https://developers.facebook.com/tools/explorer/145634995501895/ permet de tester ses requête à l'API Facebook

(Pas de besoin que le compte dev ou l'app soit lié à la page)

Pour récupérer l'id d'une page facebook :
- Dans l'API Graph coller cette requete mapage?fields=id,name ou mapage est le nom de la page. ex.: WarmUp.BD?fields=id,name
- Sinon plus compliqué ouvrir une image de la page dans un nouvel onglet, l'id est contenu dans l'url

#### Twitter

1. Créer une nouvelle App avec un compte ici https://apps.twitter.com/
2. Récupérer les keys et access tokens
3. Récupérer le nom du compte ex.: https://twitter.com/AstuceSPIP c'est AstuceSPIP
4. Les renseigner dans /ecrire/?exec=configurer_flux_rs

#### Instagram

https://www.instagram.com/developer/clients/manage/
