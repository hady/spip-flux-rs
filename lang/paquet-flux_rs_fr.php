<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// F
	'flux_rs_description' => 'Permettre de se connecter aux API des réseaux sociaux pour remonter les posts les plus récents sur votre site Internet',
	'flux_rs_nom' => 'Flux des principaux réseaux sociaux',
	'flux_rs_slogan' => '',
);
